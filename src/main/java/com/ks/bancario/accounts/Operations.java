package com.ks.bancario.accounts;

/**
 * Created by migue on 29/11/2016.
 */
public interface Operations
{
    public boolean addBalance(float amount);
    public boolean subStract(float amount);
}
