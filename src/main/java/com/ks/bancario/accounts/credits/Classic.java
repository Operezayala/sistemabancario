package com.ks.bancario.accounts.credits;

import com.ks.bancario.accounts.Operations;

/**
 * Created by migue on 16/11/2016.
 */
public class Classic extends Credit implements Operations
{

    public Classic()
    {
        balance = 100000;
        interes = 10;
        limit = 100000;
    }

    public boolean addBalance(float amount)
    {
        if (balance + amount > limit)
        {
            return false;
        }
        else
        {
            balance += amount;
            return true;
        }
    }

    @Override
    public boolean subStract(float amount)
    {
        if (balance - amount < 0)
        {
            return false;
        }
        else
        {
            balance -= amount;
            super.subStract(amount);
            return true;
        }
    }
}
