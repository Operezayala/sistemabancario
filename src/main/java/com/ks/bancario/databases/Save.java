package com.ks.bancario.databases;

import com.ks.bancario.accounts.Account;
import com.ks.bancario.person.Client;
import com.sun.org.apache.bcel.internal.generic.VariableLengthInstruction;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by migue on 08/12/2016.
 */
public class Save
{
    private static DbConnection connections;
    private static Queue<Client> VMcolaClients;
    private static Thread VMhiloProcesar;

    static
    {
        VMcolaClients = new LinkedList<Client>();
        VMhiloProcesar = new Thread();
    }

    public static DbConnection getConnection()
    {
        return connections;
    }

    public static void setConnection(DbConnection connection)
    {
        connections = connection;
    }

    public boolean saveClient(final Client client)
    {
        synchronized (VMcolaClients)
        {
            VMcolaClients.add(client);
        }
        if (!VMhiloProcesar.isAlive())
        {
            VMhiloProcesar = new Thread()
            {
                @Override
                public void run()
                {
                    String cliente;
                    Client miCliente;

                    while (VMcolaClients.size() > 0)
                    {
                        synchronized (VMcolaClients)
                        {
                            miCliente = VMcolaClients.poll();
                        }
                        if (Save.getConnection().getClass().equals(Txt.class))
                        {
                            cliente = miCliente.getName() + "|" + miCliente.getAge();
                        }
                        else if (Save.getConnection().getClass().equals(SQLlite.class))
                        {
                            cliente = "INSERT INTO tusers () VALUES ('" + miCliente.getName() + "')";
                        }
                        else
                        {
                            cliente = miCliente.getName() + "|" + miCliente.getAge();
                        }
                        Save.getConnection().connect();
                        Save.getConnection().insert(cliente);
                    }
                }
            };
            VMhiloProcesar.setDaemon(false);
            VMhiloProcesar.start();
        }


        return true;
    }

    public boolean saveAccount(Account account)
    {
        return false;
    }
}
